package org.dev4win.mewking.AntiPvPFlight;

import org.bukkit.Location;
import org.bukkit.util.Vector;

/**
 * @author mewking
 */
public class AntiZone {

    private Location a;
    private Location b;
    private Location mid;

    public AntiZone(Location a, Location b) {
        this.a = a;
        this.b = b;
        this.mid = new Location(a.getWorld(), Math.max(a.getX(), b.getX()) - ((Math.abs(a.getX() - b.getX())) / 2.0D), 0, Math.max(a.getZ(), b.getZ()) - ((Math.abs(a.getZ() - b.getZ())) / 2.0D));
    }

    public Vector pushAway(Location where, double knockback) {
        Location mc = mid.clone();
        mc.setY(where.getY() + 1);
        Vector unitVector = mc.toVector().subtract(where.toVector()).normalize();
        return unitVector.multiply(-1 * knockback);
    }

    public boolean isInside(Location c) {
        double ax = Math.min(a.getBlockX(), b.getBlockX());
        double ay = Math.min(a.getBlockY(), b.getBlockY());
        double az = Math.min(a.getBlockZ(), b.getBlockZ());
        double bx = Math.max(b.getBlockX(), a.getBlockX());
        double by = Math.max(b.getBlockY(), a.getBlockY());
        double bz = Math.max(b.getBlockZ(), a.getBlockZ());
        if (c.getBlockX() >= ax && c.getBlockX() <= bx) {
            if (c.getBlockY() >= ay && c.getBlockY() <= by) {
                if (c.getBlockZ() >= az && c.getBlockZ() <= bz) {
                    return true;
                }
            }
        }
        return false;
    }
}
