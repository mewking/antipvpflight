package org.dev4win.mewking.AntiPvPFlight;

import com.sk89q.worldedit.bukkit.WorldEditPlugin;
import com.sk89q.worldedit.bukkit.selections.Selection;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.entity.Projectile;
import org.bukkit.entity.Tameable;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.player.PlayerMoveEvent;
import org.bukkit.plugin.java.JavaPlugin;

import java.util.HashMap;
import java.util.Map;

/**
 * AntiPvPFlight - Bukkit plugin
 * Copyright (C) 2013 mewking
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
public class AntiPvPFlight extends JavaPlugin implements Listener {

    private Map<String, AntiZone> antiZones = new HashMap<String, AntiZone>();
    private Map<String, Long> cooldown = new HashMap<String, Long>();
    private Map<String, Long> cooldownMSG = new HashMap<String, Long>();

    @Override
    public void onEnable() {
        getConfig().options().copyDefaults(true);
        saveConfig();
        getServer().getPluginManager().registerEvents(this, this);
        loadZones();
        getLogger().info("enabled AntiPvPFlight!");
    }

    private void loadZones() {
        antiZones.clear();
        if (!getConfig().isSet("reg")) {
            return;
        }
        for (String se : getConfig().getConfigurationSection("reg").getKeys(false)) {
            se = "reg." + se;
            if (Bukkit.getWorld(getConfig().getString(se + ".world")) == null) {
                continue;
            }
            Location a = new Location(Bukkit.getWorld(getConfig().getString(se + ".world")), getConfig().getDouble(se + ".mx"), getConfig().getDouble(se + ".my"), getConfig().getDouble(se + ".mz"));
            Location b = new Location(Bukkit.getWorld(getConfig().getString(se + ".world")), getConfig().getDouble(se + ".xx"), getConfig().getDouble(se + ".xy"), getConfig().getDouble(se + ".xz"));
            AntiZone az = new AntiZone(a, b);
            antiZones.put(se, az);
        }
    }

    @EventHandler
    public void onPlayerMove(PlayerMoveEvent e) {
        if (!cooldown.containsKey(e.getPlayer().getName())) {
            return;
        }
        if (System.currentTimeMillis() >= cooldown.get(e.getPlayer().getName())) {
            return;
        }
        for (AntiZone az : antiZones.values()) {
            if (az.isInside(e.getTo())) {
                e.getPlayer().setVelocity(az.pushAway(e.getPlayer().getLocation(), getConfig().getDouble("knockback")));
                if (getConfig().getBoolean("renewcooldownonknockback")) {
                    cooldown.put(e.getPlayer().getName(), System.currentTimeMillis() + getConfig().getInt("cooldown") * 1000L);
                }
                if (!cooldownMSG.containsKey(e.getPlayer().getName()) || System.currentTimeMillis() >= cooldownMSG.get(e.getPlayer().getName())) {
                    e.getPlayer().sendMessage(ChatColor.translateAlternateColorCodes('&', getConfig().getString("messageonknockback")));
                    cooldownMSG.put(e.getPlayer().getName(), System.currentTimeMillis() + 1000L);
                }
                return;
            }
        }
    }

    @EventHandler(priority=EventPriority.MONITOR)
    public void onEntityDamageByEntity(EntityDamageByEntityEvent e) {
        if (e.isCancelled()) {
            return;
        }
        if (e.getEntity() instanceof Player) {
            Player damager = null;
            if (e.getDamager() instanceof Tameable) {
                if (((Tameable) e.getDamager()).isTamed()) {
                    Tameable t = (Tameable) e.getDamager();
                    if (t.getOwner() != null && t.getOwner() instanceof Player) {
                        damager = (Player) t.getOwner();
                    }
                }
            }
            if (e.getDamager() instanceof Player) {
                damager = (Player) e.getDamager();
            }
            if (e.getDamager() instanceof Projectile) {
                damager = (Player) ((Projectile) e.getDamager()).getShooter();
            }
            if (damager != null) {
                Player entity = (Player) e.getEntity();
                cooldown.put(entity.getName(), System.currentTimeMillis() + getConfig().getInt("cooldown") * 1000L);
                cooldown.put(damager.getName(), System.currentTimeMillis() + getConfig().getInt("cooldown") * 1000L);
            }
        }
    }

    @Override
    public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
        if (cmd.getName().equalsIgnoreCase("antipvpflight")) {
            if (!(sender instanceof Player)) {
                sender.sendMessage("You can only use this command as a player.");
                return true;
            }
            if (sender.hasPermission("antipvpflight.set")) {
                Player sp = (Player) sender;
                Selection s = getSelection(sp);
                if (s != null) {
                    if (args.length == 2) {
                        if (args[1].equalsIgnoreCase("delete")) {
                            String n = args[0];
                            getConfig().set("reg._" + n + "_", null);
                            saveConfig();
                            sp.sendMessage("\u00a7aRegion deleted.");
                            loadZones();
                        } else if (args[1].equalsIgnoreCase("set")) {
                            String n = args[0];
                            getConfig().set("reg._" + n + "_.mx", s.getMinimumPoint().getX());
                            getConfig().set("reg._" + n + "_.my", s.getMinimumPoint().getY());
                            getConfig().set("reg._" + n + "_.mz", s.getMinimumPoint().getZ());
                            getConfig().set("reg._" + n + "_.xx", s.getMaximumPoint().getX());
                            getConfig().set("reg._" + n + "_.xy", s.getMaximumPoint().getY());
                            getConfig().set("reg._" + n + "_.xz", s.getMaximumPoint().getZ());
                            getConfig().set("reg._" + n + "_.world", s.getMaximumPoint().getWorld().getName());
                            saveConfig();
                            sp.sendMessage("\u00a7aRegion set.");
                            loadZones();
                        }
                    } else {
                        StringBuilder sb = new StringBuilder("\u00a7eRegions: ");
                        for (String se : getConfig().getConfigurationSection("reg").getKeys(false)) {
                            sb.append(se + ", ");
                        }
                        sp.sendMessage(sb.toString());
                        return true;
                    }
                } else {
                    sender.sendMessage("\u00a7cPlease make a WorldEdit selection.");
                    return true;
                }
            } else {
                sender.sendMessage("\u00a7cYou may not use this command.");
                return true;
            }
        }
        return true;
    }

    public Selection getSelection(Player p) {
        WorldEditPlugin worldEdit = (WorldEditPlugin) Bukkit.getServer().getPluginManager().getPlugin("WorldEdit");
        Selection selection = worldEdit.getSelection(p);
        return selection;
    }

    @Override
    public void onDisable() {
        getLogger().info("disabled AntiPvPFlight!");
    }
}
